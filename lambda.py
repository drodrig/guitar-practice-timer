def lambda_handler(event, context):
    # Log the received event
    print("Received event: " + json.dumps(event, indent=2))

    try:
        params = {'key': '<your-key-from-index.php>'}

        req = urllib2.Request("<url-of-index.php>", urllib.urlencode(params))
        res = urllib2.urlopen(req)
        data = res.read()
        print(data)
    except Exception as e:
        print(e)
        message = 'Error getting Batch Job status'
        print(message)
        raise Exception(message)
