<?php

/**
 * Start / stop guitar practice timer, log to Google Sheets.
 * Run from nginx / apache / whatever.
 */

require __DIR__ . '/vendor/autoload.php';

$spreadsheetId = '<google-spreadsheet-id>';
$timerStatusFile = 'guitar-practice-timer-status';
$sheetName = Date('M Y'); 
$templateId = 0;
$to = '<your-email>';
$from

$key = '<random-chars-same-on-amazon-script>';
if (!array_key_exists('key', $_POST) || ($_POST['key'] != $key)) {
    echo 'Invalid request.';
    exit;
}

/*
 * Do some IoT wizardry here.
 */

/*
mail($to, 'IoT Button Pressed', 'Woohoo!', "From: $from\r\n");
print 'Message sent.';
 */

/**
 * Returns current state of timer ('stopped', 'started')
 */
function timerStatus() {
    global $timerStatusFile;
    $file = expandHomeDirectory($timerStatusFile);
    if (!file_exists($file)) {
        return array('stopped', 0); 
    }   
    $lastUpdate = filemtime($file);
    $f = fopen($timerStatusFile, 'r');
    $status = fread($f, 1024);
    return array($status, $lastUpdate);
}

/**
 * Update timer status, return new status.
 */
function updateTimerStatus() {
    global $timerStatusFile;
    list($timerStatus, $lastUpdate) = timerStatus();
    if ($timerStatus == 'stopped') {
        $timerStatus = 'started';
    } else {
        $timerStatus = 'stopped';
    }   
    $file = expandHomeDirectory($timerStatusFile);
    $f = fopen($file, 'w');
    fwrite($f, $timerStatus);
    fclose($f);
    return array($timerStatus, $lastUpdate);
}

/**
 * Returns an authorized API client.
 * @return Google_Client the authorized client object
 */
function getClient()
{
    $client = new Google_Client();
    $client->setApplicationName('Google Sheets API PHP Quickstart');
    $client->setScopes(['https://www.googleapis.com/auth/spreadsheets']);
    $client->setAuthConfig('client_secret.json');
    $client->setAccessType('offline');

    // Load previously authorized credentials from a file.
    $credentialsPath = expandHomeDirectory('credentials.json');
    if (file_exists($credentialsPath)) {
        $accessToken = json_decode(file_get_contents($credentialsPath), true);
    } else {
        // Request authorization from the user.
        $authUrl = $client->createAuthUrl();
        printf("Open the following link in your browser:\n%s\n", $authUrl);
        print 'Enter verification code: ';
        $authCode = trim(fgets(STDIN));

        // Exchange authorization code for an access token.
        $accessToken = $client->fetchAccessTokenWithAuthCode($authCode);

        // Store the credentials to disk.
        if (!file_exists(dirname($credentialsPath))) {
            mkdir(dirname($credentialsPath), 0700, true);
        }
        file_put_contents($credentialsPath, json_encode($accessToken));
        printf("Credentials saved to %s\n", $credentialsPath);
    }
    $client->setAccessToken($accessToken);

    // Refresh the token if it's expired.
    if ($client->isAccessTokenExpired()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
    }
    return $client;
}

/**
 * Expands the home directory alias '~' to the full path.
 * @param string $path the path to expand.
 * @return string the expanded path.
 */
function expandHomeDirectory($path)
{
    $homeDirectory = getenv('HOME');
    if (empty($homeDirectory)) {
        $homeDirectory = getenv('HOMEDRIVE') . getenv('HOMEPATH');
    }
    return str_replace('~', realpath($homeDirectory), $path);
}

/* 
 * MAIN EXECUTION STARTS
 */

// Set up the API
// Get the API client and construct the service object.
$client = getClient();
$service = new Google_Service_Sheets($client);

// Is timer stopped or started?
list($timerStatus, $lastUpdate) = updateTimerStatus();

try {
    // Get sheet values
    $response = $service->spreadsheets_values->get($spreadsheetId, $sheetName . '!A2:E');

    // Get sheet id
    $r = $service->spreadsheets->get($spreadsheetId);
    foreach ($r->sheets as $sheet) {
        if ($sheet->properties->title == $sheetName) {
            $sheetId = $sheet->properties->sheetId;
            break;
        }
    }
} catch(Exception $e) {
    // Sheet does not exist
    $requestBody = new Google_Service_Sheets_CopySheetToAnotherSpreadsheetRequest();
    $requestBody->destinationSpreadsheetId = $spreadsheetId;
    $response = $service->spreadsheets_sheets->copyTo($spreadsheetId, $templateId, $requestBody);
    $updateSheetPropertiesRequest = new Google_Service_Sheets_UpdateSheetPropertiesRequest([
        'properties' => [
            'title' => $sheetName,
            'sheetId' => $response->sheetId
        ],
        'fields' => 'title'
    ]);
    $batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest([
        'requests' => [
            new Google_Service_Sheets_Request(
                ['updateSheetProperties' => $updateSheetPropertiesRequest
            ])
        ]
    ]);
    $response = $service->spreadsheets->batchUpdate($spreadsheetId, $batchUpdateRequest);
    $sheetId = $response->sheetId;
}

$cellValues = array();
if ($timerStatus == 'started') {
    // Append row.
    $values = array(
        date('F d Y'),
        date('H:i:s')
    );
    foreach ($values as $value) {
        $cellData = new Google_Service_Sheets_CellData();
        $cellValue = new Google_Service_Sheets_ExtendedValue();
        $cellValue->setStringValue($value);
        $cellData->setUserEnteredValue($cellValue);
        $cellValues[] = $cellData;
    }
    // Build the RowData
    $rowData = new Google_Service_Sheets_RowData();
    $rowData->setValues($cellValues);

    // Prepare the request
    $append_request = new Google_Service_Sheets_AppendCellsRequest();
    $append_request->setSheetId($sheetId);
    $append_request->setRows($rowData);
    $append_request->setFields('userEnteredValue');

    // Set the request
    $request = new Google_Service_Sheets_Request();
    $request->setAppendCells($append_request);

    // Add the request to the requests array
    $requests = array();
    $requests[] = $request;

    // Prepare the update
    $batchUpdateRequest = new Google_Service_Sheets_BatchUpdateSpreadsheetRequest(array(
        'requests' => $requests
    ));

    try {
        $response = $service->spreadsheets->batchUpdate($spreadsheetId, $batchUpdateRequest);
    } catch (Exception $e) {
        error_log($e->getMessage());
    }

} else {
    $response = $service->spreadsheets_values->get($spreadsheetId, $sheetName . '!A1:E');
    $values = $response->getValues();
    $lastRowIdx = count($values) - 1;
    $duration = time() - $lastUpdate;

    $values = array(
        $values[$lastRowIdx][0],
        $values[$lastRowIdx][1],
        date('H:i:s'),
        floor($duration / 60)
    );

    $service = new Google_Service_Sheets($client);
    $range = $sheetName . '!A' . max(4, $lastRowIdx+1) . ':D';
    $valueRange = new Google_Service_Sheets_ValueRange();
    $valueRange->setValues(['values' => $values]);
    $conf = ['valueInputOption' => 'RAW'];
    try {
        $service->spreadsheets_values->update($spreadsheetId, $range, $valueRange, $conf);
    } catch (Exception $e) {
        print_r($e->getMessage());
    }
}

$ts = $timerStatus;
if ($timerStatus == 'stopped') {
    $minutes = $duration / 60;
    $dStr = floor($minutes);
    $ts .= ' (' . $dStr . ' minutes)';
}
mail($to, 'Guitar Practice', 'Timer ' . $ts . '.', "From: $from\r\n");
print 'Timer ' . $ts . '.';
