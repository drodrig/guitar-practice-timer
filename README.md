## Amazon IoT. Click button and log guitar practice.

### Purpose

Using an [Amazon IoT device](https://www.amazon.com/AWS-IoT-Button-2nd-Generation/dp/B01KW6YCIM/), log practice minutes for guitar (or whatever).

### Requirements

* PHP-enabled Web server
* AWS IoT device
* Amazon AWS account

### How to Use

1. Copy [this file](https://docs.google.com/spreadsheets/d/1V3YzS8U5yUxnJ-K7nKe6ZcSET4fthbIUfYk-hFcsmSU/edit?usp=sharing) to your Google Docs:
2. Copy index.php to your web server. Update variables at top of file (your $spreadsheetId is embedded in the Google Sheets URL).
3. Install [Google Sheets PHP API](https://developers.google.com/sheets/api/quickstart/php) to your web server.
4. [Purchase](https://www.amazon.com/AWS-IoT-Button-2nd-Generation/dp/B01KW6YCIM/) and [configure](https://docs.aws.amazon.com/iot/latest/developerguide/configure-iot.html) an IoT device. Set up as per [this article](https://console.aws.amazon.com/lambda/home):
5. Copy lambda.py to a newly created blank AWS Lambda function [here](https://console.aws.amazon.com/lambda/home) (use Python 2.7 for language). Add AWS IoT as trigger.
6. Set <your-key-from-index.php> and <url-of-index.php> in above file.
7. Click the button, watch your spreadsheet update. Program will create new sheet, so check your tabs at bottom of spreadsheet.

![Example](example.png "Example Spreadsheet")

